//
// Created by rainier on 14.02.22.
//

#ifndef HACKVM_VMTRANSLATOR_H
#define HACKVM_VMTRANSLATOR_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <memory>
#include <filesystem>

namespace hack::vm
{
    struct Assembly : std::enable_shared_from_this<Assembly>
    {
        std::vector<std::string> commands;

        void PushCommands(const std::vector<std::string>& commands_vec);
        void PushIncrementStackCmd();
        void PushDecrementStackCmd();
        void AddAssembly(std::shared_ptr<Assembly> prog);

    };

    struct VirtualMachine
    {
        std::vector<std::string> commands;
    };


    /**
     * Initialises workspace. This function should be called if any other function from this file will be used.
     * @param msg - message to be printed into stdout
     */
    void Init(const std::string& msg = {});

    /**
     * Parses vm program into assembly program text
     * @param prg - program to be parsed
     * @return Assembly program
     */
    std::shared_ptr<Assembly> ParseVMIntoASM(std::shared_ptr<VirtualMachine> prg);

    /**
     * Parses vm command into assembly set of commands
     * @param cmd - vm command to be parsed
     * @return Set of assembly commands
     */
    std::shared_ptr<Assembly> ParseVMIntoASM(const std::string& cmd);

    /**
     * Reads supplied file with virtual machine language
     * @param VMCodePath - path of vm file to be read
     * @return Virtual Machine program text
     */
    std::shared_ptr<VirtualMachine> ReadVMFile(std::filesystem::path& VMCodePath);

    /**
     * Writes Assembly program into file
     * @param prg - assembly program to be written into file
     * @param outPath - path of the file to write assembly program
     */
    void WriteASM(std::shared_ptr<Assembly> prg, const std::filesystem::path& outPath);
}

#endif //HACKVM_VMTRANSLATOR_H
