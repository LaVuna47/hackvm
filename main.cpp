#include <iostream>
#include "VMTranslator.h"
#include <filesystem>
/**
 * Parses .vm file and generates equivalent .asm file designed to run on hack platform
 * Usage:
 *      HackVM file.vm
 *  in: name of file that contains virtual machine code, should have .vm extension
 *  out: file.asm which contains assembly code.
 */

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        std::cout << "Incorrect number of arguments.\n";
        std::cout << "Specify file with vm code.\n";
        return 0;
    }

    try
    {
        std::filesystem::path vmPath(argv[1]);
        if(!std::filesystem::exists(vmPath))
        {
            throw std::runtime_error("File " + vmPath.string() + " does not exist.");
        }
        
        hack::vm::Init();
        auto vm_prg = hack::vm::ReadVMFile(vmPath);
        auto asm_prg = hack::vm::ParseVMIntoASM(vm_prg);
        // delete extension
        vmPath.replace_extension(".asm");
        hack::vm::WriteASM(asm_prg, vmPath);
        std::cout << "Success\n";
    }
    catch (std::runtime_error& e)
    {
        std::cerr << "Runtime error exception occurred:" << e.what() << "\n";
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception occurred:" << e.what() << "\n";
    }
    catch(...)
    {
        std::cerr << "Unknown Exception\n";
    }

}
