//
// Created by rainier on 14.02.22.
//

#include <optional>
#include <unordered_map>
#include "VMTranslator.h"
#include <string>
#include <algorithm>
#include <iostream>

/* Only adds one cmd into cmd */
#define ADD_ASSEMBLY_CMD(var, ...) \
{ \
 std::stringstream ss;               \
 ss << var << __VA_ARGS__;           \
 assembly->PushCommands({ss.str()}); \
}



namespace hack::vm::impl
{
    using namespace hack::vm;
    /* Command type */
    enum class CMDType
    {
        add_,
        sub_,
        neg_,
        eq_,
        get_,
        lt_,
        and_,
        or_,
        not_,
        push_,
        pop_,
        UNKNOWN_
    };

    CMDType ParseCMDType(const std::string& cmdTypeStr);
    std::string ToString(CMDType type);

    enum class SegmentType
    {
        LOCAL_,
        ARGUMENT_,
        THIS_,
        THAT_,
        CONSTANT_,
        STATIC_,
        POINTER_,
        TEMP_,
        UNKNOWN_
    };

    SegmentType ParseSegmentType(const std::string& segTypeStr);
    std::string ToString(SegmentType type);

    struct VMCommand
    {
        CMDType cmdType{};
        std::optional<SegmentType> segType;
        std::optional<uint32_t> number;
    };

    /**
     * Throws std::runtime_error if command is not valid
     * @param cmd - command to be parsed
     * @return VMCommand structure which represents c++ representation of that command
     */
    VMCommand ParseVMCommand(const std::string& cmd);

    /* Translates Push command */
    std::shared_ptr<Assembly> HandlePushCommand(SegmentType segType, uint32_t arg);
    /* Translates Pop command */
    std::shared_ptr<Assembly> HandlePopCommand(SegmentType segType, uint32_t arg);
    /* Translates arithmetical and logical commands command */
    std::shared_ptr<Assembly> HandleALUCommand(CMDType cmdType);

}

/* ===== Assembly implementation ===== */
void hack::vm::Assembly::PushCommands(const std::vector<std::string> &commands_vec)
{
    for(const auto& cmd: commands_vec)
        this->commands.emplace_back(cmd);
}

void hack::vm::Assembly::PushIncrementStackCmd()
{
    this->PushCommands({"@ST", "M=M+1"});
}

void hack::vm::Assembly::PushDecrementStackCmd()
{
    this->PushCommands({"@ST", "M=M-1"});
}

void hack::vm::Assembly::AddAssembly(std::shared_ptr<Assembly> prog)
{
    for(auto& item: prog->commands)
        this->commands.emplace_back(item);
}


/* =================================== */

namespace hack::vm
{
    /*
     * Static Data
     */
    std::unordered_map<std::string, uint32_t> RAMConstants;
    std::unordered_map<std::string, std::string> NameMap;
    uint32_t g_label_ctr = 0;


    /*
     * ======= Implementation of interfaces =======
     */
    void Init(const std::string& msg)
    {
        /// RamConstants Is this all necessary?
        RAMConstants["SP"] = 0, RAMConstants["LCL"] = 1, RAMConstants["ARG"] = 2, RAMConstants["THIS"] = 3,
        RAMConstants["THAT"] = 4, RAMConstants["Temp0"] = 5,RAMConstants["Temp1"] = 6, RAMConstants["Temp2"] = 7,
        RAMConstants["Temp3"] = 8,RAMConstants["Temp4"] = 9, RAMConstants["Temp5"] = 10, RAMConstants["Temp6"] = 11,
        RAMConstants["Temp7"] = 12, RAMConstants["R0"] = 0, RAMConstants["R1"] = 1,RAMConstants["R2"] = 2,
        RAMConstants["R3"] = 3, RAMConstants["R4"] = 4,RAMConstants["R5"] = 5,RAMConstants["R6"] = 6,
        RAMConstants["R7"] = 7,RAMConstants["R8"] = 8,RAMConstants["R9"] = 9,RAMConstants["R10"] = 10,
        RAMConstants["R11"] = 11,RAMConstants["R12"] = 12,RAMConstants["R13"] = 13,RAMConstants["R14"] = 14,
        RAMConstants["R15"] = 15, RAMConstants["static"] = 16;

        // NameMap
        NameMap["local"] = "LCL", NameMap["argument"] = "ARG", NameMap["this"] = "THIS", NameMap["that"] = "THAT";
    }

    std::shared_ptr<Assembly> ParseVMIntoASM(std::shared_ptr<VirtualMachine> prg)
    {
        std::shared_ptr<Assembly> asm_res = std::make_shared<Assembly>();
        for(const auto& vm_cmd: prg->commands)
        {
            asm_res->AddAssembly(ParseVMIntoASM(vm_cmd));
        }
        return asm_res;
    }

    std::shared_ptr<Assembly> ParseVMIntoASM(const std::string& cmd_str)
    {
        using namespace impl;
        VMCommand cmd = ParseVMCommand(cmd_str);
        if(cmd.cmdType == CMDType::push_)
        {
            return HandlePushCommand(cmd.segType.value(), cmd.number.value());
        }
        else if(cmd.cmdType == CMDType::pop_)
        {
            return HandlePopCommand(cmd.segType.value(), cmd.number.value());
        }
        else if(cmd.cmdType == CMDType::add_ || cmd.cmdType == CMDType::and_ || cmd.cmdType == CMDType::eq_
          || cmd.cmdType == CMDType::get_ || cmd.cmdType == CMDType::sub_ || cmd.cmdType == CMDType::neg_
          || cmd.cmdType == CMDType::lt_ || cmd.cmdType == CMDType::or_ || cmd.cmdType == CMDType::not_   )
        {
            return HandleALUCommand(cmd.cmdType);
        }

        // throws an error if command is unknown
        throw std::runtime_error("Unknown command.");
    }

    std::shared_ptr<VirtualMachine> ReadVMFile(std::filesystem::path& VMCodePath)
    {
        auto vm = std::make_shared<VirtualMachine>();

        std::fstream fd(VMCodePath, std::ios::in);
        if(!fd.is_open())
        {
            throw std::runtime_error("Could not open " + VMCodePath.string());
        }
        std::string line, line2;
        while(std::getline(fd,line))
        {
            // remove comments
            auto comment_pos = line.find("//");
            if (comment_pos != std::string::npos)
            {
                line = line.substr(0, comment_pos);
            }

            // remove \r character
            line.erase(std::remove(line.begin(), line.end(), '\r'), line.end());

            line2 = line;

            // remove whitespaces
            line.erase(std::remove(line.begin(), line.end(), ' '), line.end());

            // if not empty - add to commands
            if(line.empty())
                continue;

            vm->commands.emplace_back(line2);
        }
        return vm;
    }

    void WriteASM(std::shared_ptr<Assembly> prg, const std::filesystem::path& outPath)
    {
        std::fstream fd(outPath, std::ios::out | std::ios::trunc);
        if(!fd.is_open())
            throw std::runtime_error("Could not open " + outPath.string());

        for(const auto& item: prg->commands)
        {
            fd.write(item.c_str(),(std::streamsize)item.size());
        }
    }
}

/*
 * ====== IMPLEMENTATION ======
 */
namespace hack::vm::impl
{
    CMDType ParseCMDType(const std::string &cmdTypeStr)
    {
        CMDType res;
        if (cmdTypeStr == "add")
        {
            res = CMDType::add_;
        } else if (cmdTypeStr == "sub")
        {
            res = CMDType::sub_;
        } else if (cmdTypeStr == "neg")
        {
            res = CMDType::neg_;
        } else if (cmdTypeStr == "eq")
        {
            res = CMDType::eq_;
        } else if (cmdTypeStr == "get")
        {
            res = CMDType::get_;
        } else if (cmdTypeStr == "lt")
        {
            res = CMDType::lt_;
        } else if (cmdTypeStr == "and")
        {
            res = CMDType::and_;
        } else if (cmdTypeStr == "or")
        {
            res = CMDType::or_;
        } else if (cmdTypeStr == "not")
        {
            res = CMDType::not_;
        } else if (cmdTypeStr == "push")
        {
            res = CMDType::push_;
        } else if (cmdTypeStr == "pop")
        {
            res = CMDType::pop_;
        } else
        {
            res = CMDType::UNKNOWN_;
        }
        return res;
    }

    std::string ToString(CMDType type)
    {
        switch (type)
        {
            case CMDType::add_:
                return "add";
            case CMDType::sub_:
                return "sub";
            case CMDType::neg_:
                return "neg";
            case CMDType::eq_:
                return "eq";
            case CMDType::get_:
                return "get";
            case CMDType::lt_:
                return "lt";
            case CMDType::and_:
                return "and";
            case CMDType::or_:
                return "or";
            case CMDType::not_:
                return "not";
            case CMDType::push_:
                return "push";
            case CMDType::pop_:
                return "pop";
            case CMDType::UNKNOWN_:
                return "unknown";
        }
    }

    SegmentType ParseSegmentType(const std::string &segTypeStr)
    {
        SegmentType res;
        if (segTypeStr == "local")
        {
            res = SegmentType::LOCAL_;
        } else if (segTypeStr == "this")
        {
            res = SegmentType::THIS_;
        } else if (segTypeStr == "that")
        {
            res = SegmentType::THAT_;
        } else if (segTypeStr == "constant")
        {
            res = SegmentType::CONSTANT_;
        } else if (segTypeStr == "static")
        {
            res = SegmentType::STATIC_;
        } else if (segTypeStr == "pointer")
        {
            res = SegmentType::POINTER_;
        } else if (segTypeStr == "temp")
        {
            res = SegmentType::TEMP_;
        } else
        {
            res = SegmentType::UNKNOWN_;
        }
        return res;
    }

    std::string ToString(SegmentType type)
    {
        switch (type)
        {
            case SegmentType::LOCAL_:
                return "local";
            case SegmentType::ARGUMENT_:
                return "argument";
            case SegmentType::THIS_:
                return "this";
            case SegmentType::THAT_:
                return "that";
            case SegmentType::CONSTANT_:
                return "constant";
            case SegmentType::STATIC_:
                return "static";
            case SegmentType::POINTER_:
                return "pointer";
            case SegmentType::TEMP_:
                return "temp";
            case SegmentType::UNKNOWN_:
                return "unknown";
        }
    }

    VMCommand ParseVMCommand(const std::string &cmd)
    {
        // to be returned
        VMCommand cmdOut;

        std::stringstream ss;
        ss << cmd;
        std::string c1;
        ss >> c1;

        CMDType cmdType = ParseCMDType(c1);
        if (cmdType == CMDType::UNKNOWN_)
            throw std::runtime_error("Command " + cmd + " is not valid.");

        cmdOut.cmdType = cmdType;

        if (cmdType == CMDType::push_ || cmdType == CMDType::pop_)
        {
            std::string c2, c3;
            ss >> c2 >> c3;

            SegmentType segType = ParseSegmentType(c2);
            if (segType == SegmentType::UNKNOWN_)
                throw std::runtime_error("Command " + cmd + " is not valid.");
            cmdOut.segType = segType;

            uint32_t number = std::stoul(c3);

            if (number > 65536)
                throw std::runtime_error("Command " + cmd + " is not valid.");
            cmdOut.number = number;
        }
        return cmdOut;

    }

    std::shared_ptr<Assembly> HandlePushCommand(SegmentType segType, uint32_t arg)
    {
        auto assembly = std::make_shared<Assembly>();

        if(segType == SegmentType::LOCAL_ || segType == SegmentType::ARGUMENT_
           || segType == SegmentType::THIS_ || segType == SegmentType::THAT_)
        {
            // RAM[*SP] = RAM[s + arg];
            ADD_ASSEMBLY_CMD("@", RAMConstants[NameMap[ToString(segType)]] + arg);
            assembly->PushCommands({"D=M", "@SP", "A=M", "M=D"});
            assembly->PushIncrementStackCmd();
        }
        else if(segType == SegmentType::CONSTANT_)
        {
            // RAM[*SP] = arg
            ADD_ASSEMBLY_CMD("@", arg);
            assembly->PushCommands({"D=A", "@SP", "A=M", "M=D"});
            assembly->PushIncrementStackCmd();
        }
        else if(segType == SegmentType::STATIC_)
        {
            ADD_ASSEMBLY_CMD("@", "foo." << arg)
            assembly->PushCommands({"D=M", "@SP", "A=M", "M=D"});
            assembly->PushIncrementStackCmd();
        }
        else if(segType == SegmentType::POINTER_)
        {
            if(arg == 0)
            {
                // *SP = THIS
                assembly->commands.emplace_back("@THIS");
            }
            else
            {
                // *SP = THAT
                assembly->commands.emplace_back("@THAT");
            }
            assembly->PushCommands({"D=M", "@SP", "A=M", "M=D"});
            assembly->PushIncrementStackCmd();

        }
        else if(segType == SegmentType::TEMP_)
        {
            if(arg > 7)
            {
                throw std::runtime_error("HandlePushCommand: Argument out of range.");
            }
            // RAM[*SP] = RAM[Temp0 + arg]
            ADD_ASSEMBLY_CMD("@", "Temp" << arg)
            assembly->PushCommands({"D=M", "@SP", "A=M", "M=D"});
            assembly->PushIncrementStackCmd();
        }
        else
        {
            throw std::runtime_error("Could not HandlePushCommand: Unknown Segment Type.");
        }
        return assembly;
    }

    std::shared_ptr<Assembly> HandlePopCommand(SegmentType segType, uint32_t arg)
    {
        auto assembly = std::make_shared<Assembly>();

        if(segType == SegmentType::LOCAL_ || segType == SegmentType::ARGUMENT_
           || segType == SegmentType::THIS_ || segType == SegmentType::THAT_)
        {
            assembly->PushCommands({"@SP", "M=M-1", "@SP", "A=M", "D=M"});
            ADD_ASSEMBLY_CMD("@", RAMConstants[NameMap[ToString(segType)]] + arg);
            assembly->PushCommands({"M=D"});
        }
        else if(segType == SegmentType::CONSTANT_)
        {
            throw std::runtime_error("[pop constant] is invalid command.");
        }
        else if(segType == SegmentType::STATIC_)
        {
            // *SP--
            assembly->PushCommands({"@SP", "M=M-1", "@SP", "A=M", "D=M"});
            ADD_ASSEMBLY_CMD("@", "foo." << arg)
            assembly->PushCommands({"M=D"});
        }
        else if(segType == SegmentType::POINTER_)
        {
            // *SP--
            assembly->PushCommands({"@SP", "M=M-1", "@SP", "A=M", "D=M"});
            if(arg == 0)
                assembly->PushCommands({"@THIS"});
            else
                assembly->PushCommands({"@THAT"});
            assembly->PushCommands({"M=D"});
        }
        else if(segType == SegmentType::TEMP_)
        {
            if(arg > 7)
            {
                throw std::runtime_error("HandlePushCommand: Argument out of range.");
            }
            // *SP--
            assembly->PushCommands({"@SP", "M=M-1","@SP", "A=M", "D=M"});
            ADD_ASSEMBLY_CMD("@", "Temp" << arg);
            assembly->PushCommands({"M=D"});
        }
        else
        {
            throw std::runtime_error("Could not HandlePopCommand: Unknown Segment Type.");
        }
        return assembly;
    }

    /*
     add, sub, neg, eq, get, lt, and, or, not;
     */
    std::shared_ptr<Assembly> HandleALUCommand(CMDType cmdType)
    {
        auto assembly = std::make_shared<Assembly>();

        if(cmdType == CMDType::add_)
        {
            assembly->PushCommands({"@SP", "A=M-1", "D=M", "A=A-1", "M=M+D"});
            assembly->PushDecrementStackCmd();
        }
        else if(cmdType == CMDType::sub_)
        {
            assembly->PushCommands({"@SP", "A=M-1", "D=M", "A=A-1", "M=M-D"});
            assembly->PushDecrementStackCmd();
        }
        else if(cmdType == CMDType::neg_)
        {
            assembly->PushCommands({"@SP", "A=M-1", "M=-M"});
        }
        else if(cmdType == CMDType::eq_)
        {
            assembly->PushCommands(
                    {"@SP", "A=M-1", "D=M", "A=A-1", "DM=D-M", "@label" + std::to_string(g_label_ctr++),
                     "D;JEQ", "@SP", "A=M-1", "A=A-1", "M=0", "@label"+std::to_string(g_label_ctr++),
                     "0;JMP", "(label" + std::to_string(g_label_ctr-2)+")", "@SP", "A=M-1", "A=A-1", "M=1", "@SP",
                     "M=M-1", "(label"+std::to_string(g_label_ctr-1)+")"});
            assembly->PushDecrementStackCmd();
        }
        else if(cmdType == CMDType::get_)
        {
            assembly->PushCommands(
                    {"@SP", "A=M-1", "D=M", "A=A-1", "DM=D-M", "@label" + std::to_string(g_label_ctr++),
                     "D;JGE", "@SP", "A=M-1", "A=A-1", "M=0", "@label"+std::to_string(g_label_ctr++),
                     "0;JMP", "(label" + std::to_string(g_label_ctr-2)+")", "@SP", "A=M-1", "A=A-1", "M=1", "@SP",
                     "M=M-1", "(label"+std::to_string(g_label_ctr-1)+")"});
            assembly->PushDecrementStackCmd();
        }
        else if(cmdType == CMDType::lt_)
        {
            assembly->PushCommands(
                    {"@SP", "A=M-1", "D=M", "A=A-1", "DM=D-M", "@label" + std::to_string(g_label_ctr++),
                     "D;JLT", "@SP", "A=M-1", "A=A-1", "M=0", "@label"+std::to_string(g_label_ctr++),
                     "0;JMP", "(label" + std::to_string(g_label_ctr-2)+")", "@SP", "A=M-1", "A=A-1", "M=1", "@SP",
                     "M=M-1", "(label"+std::to_string(g_label_ctr-1)+")"});
            assembly->PushDecrementStackCmd();
        }
        else if(cmdType == CMDType::and_)
        {
            assembly->PushCommands({"@SP", "A=M-1", "D=M", "A=A-1", "M=M&D"});
            assembly->PushDecrementStackCmd();
        }
        else if(cmdType == CMDType::or_)
        {
            assembly->PushCommands({"@SP", "A=M-1", "D=M", "A=A-1", "M=M|D"});
            assembly->PushDecrementStackCmd();
        }
        else if(cmdType == CMDType::not_)
        {
            assembly->PushCommands({"@SP", "A=M-1", "M=!M"});
        }
        return assembly;
    }
}